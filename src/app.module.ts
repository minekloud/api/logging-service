import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'
import { LogModule } from './logs/logs.module'
import { SearchModule } from './search/search.module'
@Module({
  imports: [
    ScheduleModule.forRoot(),
    SearchModule,
    LogModule,
    ConfigModule.forRoot({ isGlobal: true, envFilePath: 'config.env' }),
  ],
})
export class AppModule {}
