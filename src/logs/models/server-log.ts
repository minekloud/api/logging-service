export class ServerLog {
  serverId: number
  latestTimestamp: Date
  oldestTimestamp: Date
}
