import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets'
import { Socket } from 'socket.io'
import { AuthService } from 'src/authentification/auth.service'
import { ServerService } from 'src/servers/server.service'

import { LogsService } from './logs.service'

@WebSocketGateway({ path: '/api/logging/socket.io' })
export class LogsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private readonly logsService: LogsService,
    private readonly serverService: ServerService,
    private readonly authService: AuthService,
  ) {}

  @SubscribeMessage('new-server-log')
  async onNewServerLog(client: Socket) {
    const serverLog = this.logsService.getServerLogByClient(client)
    if (!serverLog) {
      return
    }
    this.logsService.fetchNewLogByServer(client, serverLog)
  }

  @SubscribeMessage('old-server-log')
  async onOldServerLog(client: Socket) {
    const serverLog = this.logsService.getServerLogByClient(client)
    if (!serverLog) {
      return
    }
    this.logsService.fetchOldLogByServer(client, serverLog)
  }

  async handleConnection(client: Socket) {
    const query = client.handshake.query

    const jwtValid = await this.authService.isJwtValid(query.token)
    if (!jwtValid) {
      client.disconnect()
      return
    }

    if (
      !(await this.serverService.isUserAllowed(query.userId, query.serverId))
    ) {
      client.disconnect()
    }
    console.log(`New client connected with socket ${client.id}`)
    this.logsService.addServerLogToFetch(client, query.userId, query.serverId)
  }

  handleDisconnect(client: Socket) {
    this.logsService.removeServerLogToFetch(client)
  }
}
