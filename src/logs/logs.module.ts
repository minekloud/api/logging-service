import { HttpModule, Module } from '@nestjs/common'
import { AuthentificationModule } from 'src/authentification/authentification.module'
import { SearchModule } from 'src/search/search.module'
import { ServerModule } from 'src/servers/server.module'
import { LogsGateway } from './logs.gateway'
import { LogsService } from './logs.service'

@Module({
  imports: [HttpModule, ServerModule, SearchModule, AuthentificationModule],
  providers: [LogsGateway, LogsService],
})
export class LogModule {}
